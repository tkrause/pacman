package Core;

import Engine.Actor;

public class SpawnActor {
   protected Actor actor;
   protected double delay;
   protected double timeTaken;
   protected boolean respawn;

   public SpawnActor() {
   }

   public SpawnActor(Actor actor, double delay) {
      this.actor = actor;
      this.delay = delay;
   }

   public SpawnActor addTime(double delta) {
      this.timeTaken += delta;
      return this;
   }

   public boolean isTime() {
      return timeTaken >= delay;
   }

   public Actor getActor() {
      return actor;
   }

   public SpawnActor setActor(Actor actor) {
      this.actor = actor;
      return this;
   }

   public double getDelay() {
      return delay;
   }

   public SpawnActor setDelay(double delay) {
      this.delay = delay;
      return this;
   }

   public boolean isRespawn() {
      return respawn;
   }

   public SpawnActor setRespawn(boolean respawn) {
      this.respawn = respawn;
      return this;
   }
}
