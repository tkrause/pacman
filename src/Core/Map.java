package Core;

import Actor.Item.Pacdot;
import Actor.Item.PowerPellet;
import Engine.*;
import Actor.*;
import com.google.gson.Gson;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class Map {
   protected static Map instance = null;
   protected Point at = new Point(0, 0);

   protected int squareSize, sizeX, sizeY;
   protected List<Teleporter> teleporters = new ArrayList<>();
   protected Actor[][] map;
   protected SpawnPoint ghost, player;

   protected Map() {
      player = new SpawnPoint();
      player.spawn(Pacman.make());

      ghost = new SpawnPoint();
      ghost.spawn(new Red(), false, 3);
      ghost.spawn(new Orange(), false, 6);
      ghost.spawn(new Pink(), false, 9);
      ghost.spawn(new Blue(), false, 12);
      loadJson();

      Engine.instance().addActor(ScoreBoard.make());
   }

   public static Map instance() {
      if (instance == null)
         instance = new Map();
      return instance;
   }

   public static void destroy() {
      instance = null;
   }

   public void loadJson() {
      Gson gson = new Gson();
      try {
         //String path = getClass().getResource("/map.json").getFile();
         Reader reader = new InputStreamReader(getClass().getResourceAsStream("/map.json"), "UTF-8");
         JsonMap map = gson.fromJson(reader, JsonMap.class);

         this.sizeX = map.sizeX;
         this.sizeY = map.sizeY;
         this.squareSize = map.squareSize;
         this.map = new Actor[map.sizeX][map.sizeY];

         Engine engine = Engine.instance();
         engine.setSize(new Dimension(sizeX * squareSize, sizeY * squareSize));
         for (String[] s : map.actors) {
            for (String type : s) {
               Actor a = makeActor(type);
               this.map[at.x][at.y] = a;
               if (a != null)
                  engine.addActor(a);
               // Move to the next column
               at.x++;
            }
            // Move to the next row
            at.y++;
            // Reset the column
            at.x = 0;
         }
      } catch (Exception e) {
         Engine.log("Error occurred while loading map.json. " + e.getMessage(), true);
      }
   }

   public int getOffsetX() {
      return (squareSize / 2);
   }

   public int getOffsetY() {
      return (squareSize / 2);
   }

   public int getSquareSize() {
      return squareSize;
   }

   public SpawnPoint getGhostSpawnPoint() {
      return ghost;
   }

   public SpawnPoint getPlayerSpawnPoint() {
      return player;
   }

   public Point nextPoint() {
      return new Point((at.x * squareSize) + getOffsetX(), (at.y * squareSize) + getOffsetY());
   }

   public Teleporter makeTeleporter() {
      Teleporter t = new Teleporter();
      t.setLocation(nextPoint());

      for (int i = 0; i < teleporters.size(); i++) {
         Teleporter e = teleporters.get(i);
         e.link(t);
         t.link(e);
      }

      teleporters.add(t);

      return t;
   }

   public Wall makeWall(String state) {
      Point loc = nextPoint();//new Point(at.x * squareSize, at.y * squareSize);
      return (Wall) new Wall()
         .setActiveState(state)
         .setLocation(loc);
   }

   public Pacdot makePacdot() {
      Pacdot dot = new Pacdot();
      dot.setLocation(nextPoint());
      return dot;
   }

   public PowerPellet makePowerPellet() {
      PowerPellet pellet = new PowerPellet();
      pellet.setLocation(nextPoint());
      return pellet;
   }

   public Actor makeActor(String type) {
      switch (type) {
         case "owwall":
            return makeWall("owwall");
         case "wall":
            return makeWall("idle");
         case "pspawn":
            player.setLocation(nextPoint());
            return player;
         case "gspawn":
            ghost.setLocation(nextPoint());
            return ghost;
         case "teleport":
            return makeTeleporter();
         case "pdot":
            return makePacdot();
         case "ppellet":
            return makePowerPellet();
         default:
            return null;
      }
   }

   public Actor getAt(Point p) {
      if ((p.x >= 0 && p.x < sizeX) && (p.y >= 0 && p.y < sizeY))
         return this.map[p.x][p.y];
      return null;
   }

   public void snapToFloor(Actor actor) {
      Point p = squareToPrecise(preciseToSquare(actor.getLocation()));
      actor.setLocation(p.x, p.y);
   }

   public Point squareToPrecise(Point square) {
      int offset = squareSize / 2;
      return new Point((square.x * squareSize) + offset, (square.y * squareSize) + offset);
   }

   public Point preciseToSquare(PrecisePoint location) {
      int col = (int) Math.floor(location.x / squareSize);
      int row = (int) Math.floor(location.y / squareSize);

      return new Point(col, row);
   }

   public Point getCurrentSquare(Actor actor) {
      return preciseToSquare(actor.getLocation());
   }

   public Point getNextSquare(Actor actor, Direction direction) {
      Point next = getCurrentSquare(actor);
      return getNextSquare(next, direction);
   }

   public Point getNextSquare(Point next, Direction direction) {
      switch (direction) {
         case UP:
            next.y -= 1;
            break;
         case DOWN:
            next.y += 1;
            break;
         case LEFT:
            next.x -= 1;
            break;
         case RIGHT:
            next.x += 1;
            break;
      }

      return next;
   }

   public int getSizeX() {
      return sizeX;
   }

   public int getSizeY() {
      return sizeY;
   }


}
