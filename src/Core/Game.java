package Core;

import Actor.Pacman;
import Actor.ScoreBoard;
import Engine.Engine;

public class Game {
   public static void main(String[] args) {
      launch();
   }

   private static void launch() {
      Engine.instance()
//              .setDebug(true)
         .setOnDestroy(() -> {
            Map.destroy();
            Pacman.destroy();
            ScoreBoard.destroy();
         })
         .setOnEscape(() -> {
            Engine.instance().stop();
         });

      Map.instance();
      Engine.instance().start();
   }
}
