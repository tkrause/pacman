package Actor;

import Engine.Actor;
import Engine.State;

import java.awt.event.KeyEvent;
import java.util.*;
import java.util.List;

public class Wall extends Actor {
   protected static Map<String, State> states = new HashMap<>();

   public Wall() {
      super(24, 24);
   }

   @Override
   public void input(List<KeyEvent> keys, double delta) {

   }

   @Override
   public void tick(double delta) {

   }

   @Override
   public void collide(Actor actor, boolean simulated) {

   }

   @Override
   public void init() {
      this.addState("idle", "/Actor/wall.gif");
      this.addState("owwall", "/Actor/wall-one-way.gif");
   }

   @Override
   public Map<String, State> getStates() {
      return states;
   }
}
