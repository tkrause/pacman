package Actor.Item;

import Engine.Actor;
import Engine.Engine;

import java.awt.event.KeyEvent;
import java.util.List;

/**
 * Generic Game Item
 */
public abstract class Item extends Actor {
   /**
    * Set size
    */
   public Item() {
      super(24, 24);
   }

   /**
    * Non default size
    *
    * @param height
    * @param width
    */
   public Item(int height, int width) {
      super(height, width);
   }

   /**
    * No implementation for input, tick, or collision
    *
    * @param keys
    * @param delta
    */
   @Override
   public void input(List<KeyEvent> keys, double delta) {
   }

   @Override
   public void tick(double delta) {
   }

   @Override
   public void collide(Actor actor, boolean simulated) {
   }

   /**
    * On kill, we'll just remove it from the engine
    */
   public void kill() {
      Engine.instance().deleteActor(this);
   }
}
