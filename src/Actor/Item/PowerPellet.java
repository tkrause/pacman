package Actor.Item;

import Engine.State;

import java.util.HashMap;
import java.util.Map;

/**
 * Power Pellet, supposed to instance ghost vulnerable
 */
public class PowerPellet extends Item {
   // Animation states
   protected static Map<String, State> states = new HashMap<>();

   /**
    * On create load animation states
    */
   @Override
   public void init() {
      this.addState("idle", "/Actor/Item/power-pellet.gif");
   }

   /**
    * Get animation states
    *
    * @return states
    */
   @Override
   public Map<String, State> getStates() {
      return states;
   }
}
