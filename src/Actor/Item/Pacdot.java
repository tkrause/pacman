package Actor.Item;

import Engine.State;

import java.util.HashMap;
import java.util.Map;

/**
 * Pacdot Item
 */
public class Pacdot extends Item {
   // Animation states
   protected static Map<String, State> states = new HashMap<>();

   /**
    * Set pacdot size
    */
   public Pacdot() {
      super(13, 13);
   }

   /**
    * On create of item add animation states
    */
   @Override
   public void init() {
      this.addState("idle", "/Actor/Item/pacdot.gif");
   }

   /**
    * Get the animation states
    *
    * @return states
    */
   @Override
   public Map<String, State> getStates() {
      return states;
   }
}
