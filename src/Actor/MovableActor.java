package Actor;

import Core.Map;
import Engine.Actor;
import Engine.Direction;
import Engine.PrecisePoint;

import java.awt.*;

public abstract class MovableActor extends Actor {

   protected Direction oldDirection = Direction.NONE;
   protected Direction direction = Direction.NONE;
   protected Teleporter lastPortDest = null;
   protected double turnThreshold = 4;

   public MovableActor() {
      super(24, 24);
   }

   public MovableActor(int height, int width) {
      super(height, width);
   }

   public void handleCollide(Actor actor, boolean simulated) {
      if (actor instanceof Teleporter) {
         Teleporter tp = (Teleporter) actor;
         Teleporter dest = tp.getDestination();
         if (dest != null && !tp.equals(dest)) {
            if (canPort()) {
               lastPortDest = dest;
               this.setLocation(dest.getLocation().x, dest.getLocation().y);
            }
         }
      }
   }

   protected double moveDistance(Direction direction, double requested) {
      Map map = Map.instance();
      Actor next = map.getAt(map.getNextSquare(this, direction));
      double available = requested;

      if (next instanceof Wall) {
         PrecisePoint distance = this.distanceTo(next);
         // Compute the available distance
         switch (direction) {
            case UP:
               available = distance.y - map.getSquareSize();
               break;
            case DOWN:
               available = distance.y - map.getSquareSize();
               break;
            case LEFT:
               available = distance.x - map.getSquareSize();
               break;
            case RIGHT:
               available = distance.x - map.getSquareSize();
               break;
         }
      }

      return available > requested ? requested : available;
   }

   protected boolean canChangeDirection(Direction newDirection) {
      // What is the distance to the center of the current tile?
      // If it is within turn threshhold, what is the next tile in the direction
      // they want to go?
      // If it isn't a wall we can change directions. Then we need to snap to that location and
      // move in the requested direction
      if (this.direction == newDirection)
         return false;

      // If we can't port, we did it recently and we shouldn't be able to change
      // direction or we'll end up off the map
      if (!canPort())
         return false;

      Map map = Map.instance();
      Point square = map.getCurrentSquare(this);
      Point real = map.squareToPrecise(square);
      PrecisePoint distance = this.distanceTo(real.x, real.y);
      if ((distance.x + distance.y) <= turnThreshold) {
         Actor next = map.getAt(map.getNextSquare(square, newDirection));
         if (!(next instanceof Wall)) {
            return true;
         }
      }

      return false;
   }

   protected boolean canPort() {
      return lastPortDest == null;
   }

   protected void setCanPort() {
      if (lastPortDest == null)
         return;

      // Pacman must have moved off the teleporter we arrived on
      if (!this.getBoundingBox().intersects(lastPortDest.getBoundingBox())) {
         lastPortDest = null;
      }
   }
}
