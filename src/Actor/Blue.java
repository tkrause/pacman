package Actor;

import java.awt.*;
import java.util.HashMap;

import Core.Map;
import Engine.State;

/**
 * Blue ghost
 */
public class Blue extends GhostAI {
   // animation states
   protected static java.util.Map<String, State> states = new HashMap<>();

   /**
    * Get the animation states
    *
    * @return states
    */
   @Override
   public java.util.Map<String, State> getStates() {
      return states;
   }

   /**
    * Determine the destination we should try to move towards.
    *
    * @return location
    */
   @Override
   public Point destination() {
      Map map = Map.instance();
      return map.getCurrentSquare(Pacman.make());
   }

   /**
    * Load animation states on create and setup speed
    */
   @Override
   public void init() {
      this.addState("right", "/Actor/Inky/right.gif");
      this.addState("left", "/Actor/Inky/left.gif");
      this.addState("up", "/Actor/Inky/up.gif");
      this.addState("down", "/Actor/Inky/down.gif");
      this.addState("vulnerable", "/Actor/ghost-vulnerable.gif");
      this.setActiveState("right");
      this.setHandlesCollision(true);
      this.setSpeed(108);
   }
}
