package Actor;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Map;

import Engine.Actor;
import Engine.State;

public class ScoreBoard extends Actor {

   protected static int score = 0, lives;
   protected static ScoreBoard scoreboard = null;

   public static ScoreBoard make() {
      if (scoreboard == null) {
         scoreboard = new ScoreBoard();
      }
      return scoreboard;
   }

   private ScoreBoard() {
      super(32, 75);
   }

   //Adds amount of points passed into the function to the score variable.
   public static void addScore(int s) {
      score = score + s;
   }

   public static int getScore() {
      return score;
   }

   public static void setLives(int l) {
      lives = l;
   }

   @Override
   public void init() {
      // TODO Auto-generated method stub
      this.setLocation(15, 20);
   }

   @Override
   public void input(List<KeyEvent> keys, double delta) {
      // TODO Auto-generated method stub

   }

   @Override
   public void tick(double delta) {
      // TODO Auto-generated method stub

   }

   @Override
   public void collide(Actor actor, boolean simulated) {
      // TODO Auto-generated method stub

   }

   @Override
   public Map<String, State> getStates() {
      return null;
   }

   @Override
   public void render(Graphics2D g) {
      g.setFont(new Font("TimesRoman", Font.BOLD, 20));
      g.setColor(Color.white);
      g.drawString("Lives: " + lives + " Score: " + score, (int) this.getLocation().x, (int) this.getLocation().y);
   }

   public static void destroy() {
      score = 0;
      lives = 0;
      scoreboard = null;
   }
}
