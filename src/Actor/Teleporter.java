package Actor;

import Engine.Actor;
import Engine.State;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Teleporter extends Actor {
   protected static List<Teleporter> links;

   public Teleporter() {
      super(24, 24);
   }

   @Override
   public void init() {
      links = new ArrayList<>();
   }

   @Override
   public void input(List<KeyEvent> keys, double delta) {
   }

   @Override
   public void tick(double delta) {
   }

   @Override
   public void collide(Actor actor, boolean simulated) {
   }

   @Override
   public Map<String, State> getStates() {
      return null;
   }

   public void link(Teleporter t) {
      // Do not allow links to itself
      if (t.equals(this))
         return;
      links.add(t);
   }

   public Teleporter getDestination() {
      if (links.size() <= 0)
         return null;

      int index = (int) (Math.random() * links.size());
      return links.get(index);
   }
}