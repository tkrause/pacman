package Actor;

import java.awt.*;
import java.util.HashMap;

import Core.Map;
import Engine.State;


public class Orange extends GhostAI {
   protected static java.util.Map<String, State> states = new HashMap<>();

   @Override
   public java.util.Map<String, State> getStates() {
      return states;
   }


   @Override
   public Point destination() {
      Map map = Map.instance();
      return map.getCurrentSquare(Pacman.make());
   }

   @Override
   public void init() {
      this.addState("right", "/Actor/Clyde/right.gif");
      this.addState("left", "/Actor/Clyde/left.gif");
      this.addState("up", "/Actor/Clyde/up.gif");
      this.addState("down", "/Actor/Clyde/down.gif");
      this.addState("vulnerable", "/Actor/ghost-vulnerable.gif");
      this.setActiveState("right");
      this.setHandlesCollision(true);
      this.setSpeed(100);
   }
}