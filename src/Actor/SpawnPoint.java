package Actor;

import Core.SpawnActor;
import Engine.Actor;
import Engine.Engine;
import Engine.State;

import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class SpawnPoint extends Actor {
   protected CopyOnWriteArrayList<SpawnActor> queue
      = new CopyOnWriteArrayList<>();

   public SpawnPoint() {
      super(24, 24);
   }

   @Override
   public void init() {

   }

   @Override
   public void input(List<KeyEvent> keys, double delta) {

   }

   @Override
   public void tick(double delta) {
      for (int i = 0; i < queue.size(); i++) {
         SpawnActor actor = queue.get(i);
         actor.addTime(delta);
         if (actor.isTime()) {
            actor.getActor().setLocation(location.x, location.y);
            if (!actor.isRespawn()) {
               Engine.instance().addActor(actor.getActor());
            }
            queue.remove(i);
         }
      }
   }

   @Override
   public void collide(Actor actor, boolean simulated) {

   }

   @Override
   public Map<String, State> getStates() {
      return null;
   }

   public void spawn(Actor actor) {
      spawn(actor, false, 0);
   }

   public void spawn(Actor actor, boolean respawn) {
      spawn(actor, respawn, 0);
   }

   public void spawn(Actor actor, boolean respawn, double delay) {
      queue.add(new SpawnActor(actor, delay).setRespawn(respawn));
   }
}
