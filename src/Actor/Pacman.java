package Actor;

import Actor.Item.Item;
import Actor.Item.Pacdot;
import Actor.Item.PowerPellet;
import Engine.*;
import Core.Map;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;

public class Pacman extends MovableActor {
   protected static java.util.Map<String, State> states = new HashMap<>();
   protected Direction oldDirection = Direction.NONE;
   protected Direction direction = Direction.NONE;
   protected int lives;
   protected boolean changedDirection = false;
   private static Pacman instance = null;

   public static Pacman make() {
      if (instance == null) {
         instance = new Pacman();
      }
      return instance;
   }

   @Override
   public void input(List<KeyEvent> keys, double delta) {
      if (isDeadState())
         return;

      for (KeyEvent key : keys) {
         switch (key.getKeyCode()) {
            case KeyEvent.VK_UP:
               setDirection(Direction.UP);
               break;
            case KeyEvent.VK_DOWN:
               setDirection(Direction.DOWN);
               break;
            case KeyEvent.VK_LEFT:
               setDirection(Direction.LEFT);
               break;
            case KeyEvent.VK_RIGHT:
               setDirection(Direction.RIGHT);
               break;
         }
      }
   }

   @Override
   public void tick(double delta) {
      if (isDeadState())
         return;

      if (direction == Direction.NONE) {
         this.getState(getActiveState()).setPaused(true);
         return;
      }

      double moveDistance = delta * speed;
      if (changedDirection) {
         Map map = Map.instance();
         Point snap = map.squareToPrecise(map.preciseToSquare(this.location));
         PrecisePoint distance = this.distanceTo(snap.x, snap.y);
         switch (oldDirection) {
            case UP:
            case DOWN:
               moveDistance -= distance.y;
               break;
            case LEFT:
            case RIGHT:
               moveDistance -= distance.x;
               break;
         }
         map.snapToFloor(this);
         changedDirection = false;
      }

      if (moveDistance <= 0)
         return;

      double available = moveDistance(direction, moveDistance);
      if (available > 0) {
         move(direction, available);
         setCanPort();
      } else
         this.direction = Direction.NONE;
   }

   @Override
   public void collide(Actor actor, boolean simulated) {
      super.handleCollide(actor, simulated);

      if (isDeadState())
         return;

      if (actor instanceof Wall) {
         direction = Direction.NONE;
      }

      if (actor instanceof GhostAI) {
         kill();
      }

      if (actor instanceof Item) {
         if (actor instanceof Pacdot) {
            ScoreBoard.addScore(10);
         }
         if (actor instanceof PowerPellet) {
            ScoreBoard.addScore(50);
         }
         Item item = (Item) actor;
         item.kill();

         if (!Engine.instance().hasActor(Pacdot.class)
            && !Engine.instance().hasActor(PowerPellet.class)) {
            Engine.instance().stop();
         }
      }
   }

   @Override
   public void init() {
      // 72 points per second or 1 inch per second
      // Pacman will move 3 inches per second
      this.lives = 3;
      ScoreBoard.setLives(lives);
      this.setSpeed(144);
      this.setHandlesCollision(true);

      this.addState("idle", "/Actor/Pacman/idle.gif");
      this.addState("right", "/Actor/Pacman/right.gif");
      this.addState("left", "/Actor/Pacman/left.gif");
      this.addState("up", "/Actor/Pacman/up.gif");
      this.addState("down", "/Actor/Pacman/down.gif");
      // Death Animations
      this.addState("die-right", "/Actor/Pacman/die-right.gif", false, this::respawn);
      this.addState("die-left", "/Actor/Pacman/die-left.gif", false, this::respawn);
      this.addState("die-up", "/Actor/Pacman/die-up.gif", false, this::respawn);
      this.addState("die-down", "/Actor/Pacman/die-down.gif", false, this::respawn);
   }

   @Override
   public java.util.Map<String, State> getStates() {
      return states;
   }

   protected boolean isDeadState() {
      String state = getActiveState();
      if (state != null)
         return getActiveState().contains("die");
      return false;
   }

   protected void respawn() {
      if (lives <= 0) {
         Engine.instance().stop();
         return;
      }

      this.setActiveState("idle", true);
      direction = Direction.NONE;
      Map.instance().getPlayerSpawnPoint().spawn(this, true);
   }

   public void kill() {
      switch (direction) {
         case DOWN:
            this.setActiveState("die-down");
            break;
         case UP:
            this.setActiveState("die-up");
            break;
         case LEFT:
            this.setActiveState("die-left");
            break;
         case RIGHT:
         case NONE:
            this.setActiveState("die-right");

      }

      lives--;
      ScoreBoard.setLives(lives);
   }

   protected void setDirection(Direction direction) {
      if (this.direction != direction) {
         if (canChangeDirection(direction)) {
            this.oldDirection = this.direction;
            this.direction = direction;
            this.changedDirection = true;

            this.getState(getActiveState()).setPaused(false);
            this.setActiveState(direction.name().toLowerCase());
         }
      }
   }

   public static void destroy() {
      instance = null;
   }
}
