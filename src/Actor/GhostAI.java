package Actor;

import Core.Map;
import Engine.PrecisePoint;
import Engine.Direction;
import Engine.Actor;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.List;

public abstract class GhostAI extends MovableActor {
   public abstract Point destination();

   protected Point lastSquare = null, currentSquare = null;
   protected Direction direction = Direction.NONE;
   protected boolean changedDirection = false;

   @Override
   public void input(List<KeyEvent> keys, double delta) {
   }

   @Override
   public void collide(Actor actor, boolean simulated) {
      this.handleCollide(actor, simulated);
   }

   @Override
   public void tick(double delta) {
      // Ghost movement
      // Ghost should determine the square to go to
      // Then on tick, see what tiles are around us
      // which ever is not a wall and the shortest distance to our target
      // is the direction we will go in
      Direction newDirection = findShortestDistance();
      Direction oldDirection = this.direction;
      if (newDirection != Direction.NONE && newDirection != this.direction) {
         direction = newDirection;
         changedDirection = true;
         this.setActiveState(direction.name().toLowerCase());
      }

      double moveDistance = delta * speed;
      if (changedDirection) {
         Map map = Map.instance();
         Point snap = map.squareToPrecise(map.preciseToSquare(this.location));
         PrecisePoint distance = this.distanceTo(snap.x, snap.y);
         switch (oldDirection) {
            case UP:
            case DOWN:
               moveDistance -= distance.y;
               break;
            case LEFT:
            case RIGHT:
               moveDistance -= distance.x;
               break;
         }
         map.snapToFloor(this);
         changedDirection = false;
      }

      if (moveDistance <= 0)
         return;

      double available = moveDistance(direction, moveDistance);

      if (available > 0) {
         move(direction, available);
         Map map = Map.instance();
         Point square = map.getCurrentSquare(this);
         // If we moved off the previous square
         if (!square.equals(currentSquare)) {
            lastSquare = currentSquare;
            currentSquare = square;
            // Should happen only one time when ghost first moves
            if (lastSquare == null)
               lastSquare = currentSquare;
         }
         setCanPort();
      }
   }

   protected Direction findShortestDistance() {
      Map map = Map.instance();

      Direction[] directions = Direction.directions();
      Point destination = this.destination();

      Direction shortest = Direction.NONE;
      double shortestDistance = -1;

      for (Direction direction : directions) {
         // Ghosts cannot go in the opposite direction
         // if it the opposite, skip it
         if (direction == this.direction.inverse())
            continue;

         if (!canChangeDirection(direction))
            continue;

         // Get the square of the next location
         Point square = map.getNextSquare(map.getCurrentSquare(this), direction);
         // Do not allow us to move onto the square we came from
         if (square.equals(lastSquare))
            continue;

         // Get the actor on that square
         Actor actor = map.getAt(square);
         // If it is a wall, skip this iteration
         if (actor instanceof Wall) {
            continue;
         }

         // If not a wall, is this the shortest path?
         // Get the actual location and compare to the destination
         Point current = map.squareToPrecise(square);
         Point dest = map.squareToPrecise(destination);
         PrecisePoint distance = this.distanceTo(current, dest, false);
         double dist = Math.hypot(distance.x, distance.y);
         if (shortestDistance == -1 || dist < shortestDistance) {
            shortest = direction;
            shortestDistance = dist;
         }
      }

      return shortest;
   }
}