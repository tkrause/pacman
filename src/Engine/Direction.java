package Engine;

/**
 * Represents a 2D direction in the engine
 */
public enum Direction {
   UP, DOWN, LEFT, RIGHT, NONE;

   private Direction inverse;

   static {
      UP.inverse = DOWN;
      DOWN.inverse = UP;
      LEFT.inverse = RIGHT;
      RIGHT.inverse = LEFT;
      NONE.inverse = NONE;
   }

   public Direction inverse() {
      return inverse;
   }

   public static Direction[] directions() {
      return new Direction[]{
         UP,
         LEFT,
         DOWN,
         RIGHT
      };
   }
}
