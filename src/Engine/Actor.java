package Engine;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Color;
import java.awt.Point;

import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Map;

/**
 * Represents an Actor or Object in the game
 */
public abstract class Actor {
   //region Properties
   protected int height, width;
   protected PrecisePoint location;
   protected Rectangle boundingBox;
   protected double speed;
   protected boolean handlesCollision;

   // State properties for animations, start on the idle state
   protected int activeFrame;
   protected String activeState = "idle";
   //endregion

   //region Abstract Methods
   public abstract void init();

   public abstract void input(List<KeyEvent> keys, double delta);

   public abstract void tick(double delta);

   public abstract void collide(Actor actor, boolean simulated);

   public abstract Map<String, State> getStates();
   //endregion

   //region Constructors

   /**
    * Create an Actor
    *
    * @param height Height
    * @param width  Width
    */
   public Actor(int height, int width) {
      this(height, width, new PrecisePoint(null), null, 0);
   }

   /**
    * Create and Actor
    *
    * @param height      Height
    * @param width       Width
    * @param location    Location
    * @param boundingBox Collision Box
    * @param speed       Speed
    */
   public Actor(
      int height,
      int width,
      Point location,
      Rectangle boundingBox,
      double speed) {

      this(height, width, new PrecisePoint(location), boundingBox, speed);
   }

   /**
    * Creates an Actor
    *
    * @param height      Height
    * @param width       Width
    * @param location    Location
    * @param boundingBox Collision Box
    * @param speed       Speed
    */
   public Actor(
      int height,
      int width,
      PrecisePoint location,
      Rectangle boundingBox,
      double speed) {

      this.location = location;
      this.height = height;
      this.width = width;
      this.speed = speed;
      this.boundingBox = boundingBox;

      init();
   }
   //endregion

   //region Getters and Setters

   /**
    * Gets the Location of the Actor
    *
    * @return Actor's location
    */
   public PrecisePoint getLocation() {
      return location;
   }

   /**
    * Gets the height of the Actor
    *
    * @return Actor's height
    */
   public int getHeight() {
      return height;
   }

   /**
    * Gets the width of the Actor
    *
    * @return Actor's width
    */
   public int getWidth() {
      return width;
   }

   /**
    * Gets the Collision Box of the Actor
    *
    * @return Collision Box
    */
   public Rectangle getBoundingBox() {
      boundingBox = new Rectangle(
         (int) getTrueLocation().x,
         (int) getTrueLocation().y,
         width,
         height
      );
      return boundingBox;
   }

   /**
    * Gets the speed that the Actor has
    *
    * @return Actor's Speed
    */
   public double getSpeed() {
      return speed;
   }

   /**
    * Gets the current animation state
    *
    * @return Animation State
    */
   public String getActiveState() {
      return activeState;
   }

   /**
    * Gets a state by name
    *
    * @param key Name of the state to get
    * @return state|null
    */
   public State getState(String key) {
      Map<String, State> states = getStates();
      if (hasState(key)) {
         return states.get(key);
      }
      return null;
   }

   /**
    * Gets the index of the current frame
    *
    * @return Current frame index
    */
   public int getActiveFrame() {
      return activeFrame;
   }

   /**
    * Gets the center point or pivot point of this Actor
    *
    * @return Center of the Actor
    */
   public PrecisePoint getTrueLocation() {
      if (getHeight() == 0 || getWidth() == 0)
         return null;

      return new PrecisePoint(
         this.location.x - getHeight() / 2,
         this.location.y - getWidth() / 2
      );
   }

   /**
    * Sets whether or not this Actor should receive
    * collision notification events
    *
    * @param value true|false
    */
   public void setHandlesCollision(boolean value) {
      this.handlesCollision = value;
   }

   /**
    * Gets whether or not this Actor should receive
    * collision notification events
    *
    * @return true|false
    */
   public boolean handlesCollision() {
      return handlesCollision;
   }

   /**
    * Sets the Height of the Actor
    *
    * @param height Height
    */
   public void setHeight(int height) {
      this.height = height;
   }

   /**
    * Sets the Width of the Actor
    *
    * @param width Width
    */
   public void setWidth(int width) {
      this.width = width;
   }

   /**
    * Sets the Collision Box of the Actor
    *
    * @param boundingBox Collision Box
    */
   public void setBoundingBox(Rectangle boundingBox) {
      this.boundingBox = boundingBox;
   }

   /**
    * Sets the Speed of the Actor
    *
    * @param speed
    */
   public void setSpeed(double speed) {
      this.speed = speed;
   }

   /**
    * Checks to see if a state exists on this actor or not
    *
    * @param state Name of the state to check
    * @return true|false
    */
   public boolean hasState(String state) {
      Map<String, State> states = getStates();
      if (states != null)
         return getStates().containsKey(state);
      return false;
   }

   /**
    * Changes the state of the actor
    *
    * @param state Name of the state to change to
    * @return this actor for chaining
    */
   public Actor setActiveState(String state, boolean ignoreCallback) {
      // If we have the state and if it is not currently active
      if (hasState(state) && !getActiveState().equals(state)) {
         // Reset the old state
         if (hasState(getActiveState()))
            resetState(ignoreCallback);
         this.activeState = state;
      }

      return this;
   }

   /**
    * Changes the state of the actor
    *
    * @param state The state
    * @return this actor for chaining
    */
   public Actor setActiveState(String state) {
      setActiveState(state, false);

      return this;
   }

   /**
    * Sets the active frame for the current animation.
    *
    * @param frame
    * @return this actor for chaining
    */
   public Actor setActiveFrame(int frame) {
      this.activeFrame = frame;

      return this;
   }

   /**
    * Add a state to the actor
    *
    * @param key  State name
    * @param path Path to the resource representing this state
    * @return The State created
    */
   public State addState(String key, String path) {
      Map<String, State> states = getStates();
      State s = new State(key, path);
      states.put(key, s);

      return s;
   }

   /**
    * Add a state to the actor
    *
    * @param key    State name
    * @param path   Path to the resource representing this state
    * @param onDone OnDone callback
    * @return The State created
    */
   public State addState(String key, String path, Runnable onDone) {
      Map<String, State> states = getStates();
      State s = new State(key, path, true, onDone);
      states.put(key, s);

      return s;
   }

   /**
    * Add a state to the actor
    *
    * @param key    State name
    * @param path   Path to the resource representing this state
    * @param onDone OnDone callback
    * @return The State created
    */
   public State addState(
      String key, String path,
      boolean loop, Runnable onDone) {

      Map<String, State> states = getStates();
      State s = new State(key, path, loop, onDone);
      states.put(key, s);

      return s;
   }

   /**
    * Set the location with a new Point
    *
    * @param location Location to change to
    */
   public Actor setLocation(PrecisePoint location) {
      this.location = location;
      return this;
   }

   /**
    * Set the location with a new Point
    *
    * @param location Location to change to
    */
   public Actor setLocation(Point location) {
      this.location = new PrecisePoint(location);
      return this;
   }

   /**
    * Set the location with x and y coordinates
    *
    * @param x Horizontal location
    * @param y Vertical location
    */
   public Actor setLocation(double x, double y) {
      setLocation(new PrecisePoint(x, y));
      return this;
   }

   /**
    * Finds distance between two actors.
    *
    * @param actor
    * @return distance
    */
   public PrecisePoint distanceTo(Actor actor) {
      return distanceTo(getLocation(), actor.getLocation(), false);
   }

   /**
    * Finds distance between two actors.
    *
    * @param actor
    * @return distance
    */
   public PrecisePoint distanceTo(Actor actor, boolean sign) {
      return distanceTo(getLocation(), actor.getLocation(), sign);
   }

   /**
    * Finds distance between two actors.
    *
    * @param p1
    * @return distance
    */
   public PrecisePoint distanceTo(PrecisePoint p1) {
      return distanceTo(getLocation(), p1, false);
   }

   /**
    * Finds distance between two actors.
    *
    * @param x
    * @param y
    * @return distance
    */
   public PrecisePoint distanceTo(double x, double y) {
      return distanceTo(getLocation(), new PrecisePoint(x, y), false);
   }

   /**
    * Finds distance between two actors.
    *
    * @param p1
    * @param p2
    * @param sign
    * @return distance
    */
   public PrecisePoint distanceTo(Point p1, Point p2, boolean sign) {
      return distanceTo(new PrecisePoint(p1), new PrecisePoint(p2), sign);
   }

   /**
    * Finds distance between two actors.
    *
    * @param p1
    * @param p2
    * @param sign
    * @return distance
    */
   public PrecisePoint distanceTo(
      PrecisePoint p1, PrecisePoint p2, boolean sign) {

      if (!sign) {
         return new PrecisePoint(Math.abs(p1.x - p2.x), Math.abs(p1.y - p2.y));
      }
      return new PrecisePoint((p1.x - p2.x), (p1.y - p2.y));
   }
   //endregion

   //region Methods

   /**
    * Advanced the State to the next frame. If we do not have anymore frames
    * start at the beginning
    */
   protected Frame advanceState() {
      State current = getState(getActiveState());
      Frame f = null;
      if (current != null) {
         // Get the current frame we need to render
         f = current.getFrame(activeFrame);
         // If the state is paused, do not advance the frame
         // count
         if (current.isPaused())
            return f;

         activeFrame++;
         if (activeFrame > current.getFrameCount() - 1) {
            // If we hit the end of this state
            // can we loop?
            if (current.canLoop()) {
               // We can loop the animation reset the state
               resetState();
            } else {
               // We are at the last frame and cannot loop
               // revert to old frame
               activeFrame--;
               runOnDone();
            }
         }
      }
      return f;
   }

   /**
    * Reset the State to the first frame and run callback
    */
   protected void resetState() {
      resetState(false);
   }

   /**
    * Resets the State to the first frame
    *
    * @param ignoreCallback Should we ignore the callback
    */
   protected void resetState(boolean ignoreCallback) {
      State current = getState(getActiveState());
      if (current != null) {
         if (!ignoreCallback)
            runOnDone();
         activeFrame = 0;
      }
   }

   protected void runOnDone() {
      State current = getState(getActiveState());
      if (current.hasOnDone())
         current.getOnDone().run();
   }

   /**
    * Get the current frame and render it
    *
    * @param g Graphics
    */
   public void render(Graphics2D g) {
      // Get location for rendering which will be the center of the object
      Point p = getTrueLocation().toPoint();
      // If there is nothing to render, skip
      if (p == null)
         return;

      // Advance the frame
      Frame f = advanceState();

      // If there is no frame for whatever reason
      // log it and continue with business as usual
      if (f == null) {
         Engine.log(
            String.format(
               "Unable to obtain frame from state '%s'" +
               "on frame '%d' for Actor class '%s'",
               getActiveState(),
               getActiveFrame(),
               getClass().getName()
            )
         );
      } else {
         // Make the center of the object the relative point
         g.drawImage(f.getSprite(), p.x, p.y, getWidth(), getHeight(), null);
      }

      // If debug, draw the bounding box and center location
      if (Engine.instance().isDebug()) {
         // Draw collision box
         g.setColor(Color.WHITE);
         g.draw(getBoundingBox());

         // Draw center point
         g.setColor(Color.RED);
         g.fillOval((int) location.x - 4, (int) location.y - 4, 8, 8);
      }
   }

   /**
    * Move helper method for Actor subclasses
    * you must check if you can move before calling this
    *
    * @param direction Direction
    * @param offset    Distance to move
    */
   public void move(Direction direction, double offset) {
      switch (direction) {
         case UP:
            this.location.y -= offset;
            break;
         case DOWN:
            this.location.y += offset;
            break;
         case LEFT:
            this.location.x -= offset;
            break;
         case RIGHT:
            this.location.x += offset;
            break;
      }

      //Engine.log("Moved " + direction.toString() + " " + offset);
   }
   //endregion
}
