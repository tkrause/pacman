package Engine;

//region Imports

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.util.*;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
//endregion

/**
 * Represents the Engine for the game it handles rendering,
 * updates, collisions, input events, and FPS capping.
 * Uses the Singleton design pattern
 */
public class Engine implements KeyListener {
   //region Properties

   /**
    * There should only ever be one Engine
    * keep track of that instance here
    */
   private static Engine instance = null;
   /**
    * List of Actor instances that are registered with
    * the Engine
    */
   private CopyOnWriteArrayList<Actor> actors = new CopyOnWriteArrayList<>();
   /**
    * List of keys that are currently pressed down
    */
   private List<KeyEvent> keys = new ArrayList<>();
   /**
    * Engine will run while true
    */
   private Boolean run;
   private boolean fullscreen;
   /**
    * Calculated from the FPS when the engine is booted
    * this will be the maximum different in time between
    * renders
    */
   private double delta;
   /**
    * The component we should render to
    * Warning, the engine will take over this component completely
    * you should not put any logic in the component. Typically a JFrame
    */
   private JFrame window;
   /**
    * A callback function to run when the Engine is stopped
    */
   private Runnable onDestroy;
   /**
    * A callback function to run when the Engine is stopped
    */
   private Runnable onEscape;
   /**
    * The size of the screen will be set at Engine runtime
    */
   private Dimension screenSize = null;
   /**
    * Maximum difference between last run and current run allowed in the game loop
    */
   private static final double MAX_TIME_DIFF = 0.5;
   /**
    * Maximum number of frames we can skip before we have to force a resync
    */
   private static final int MAX_FRAME_SKIP = 5;

   private int frames = 0;
   private double currentFPS = 0;
   private double startTime;
   private double lastTick = 0;
   private boolean debug = false;

   //endregion

   //region Initialization

   /**
    * Get an Engine instance. Must create with an overload
    * of the forge method or set the component window manually.
    *
    * @return Engine|null instance
    */
   public static Engine instance() {
      return instance(null);
   }

   /**
    * Get or create an Engine instance with a custom FPS
    *
    * @return Engine|null instance
    */
   public static Engine instance(int fps) {
      return instance(null, fps);
   }

   /**
    * Get or create an Engine instance with a set window
    * and FPS set to the default 60
    *
    * @param window JFrame to takeover and render on
    * @return Engine|null instance
    */
   public static Engine instance(JFrame window) {
      return instance(window, 60);
   }

   /**
    * Get or create an Engine instance with a set window
    * and custom FPS
    *
    * @param window JFrame to takeover and render on
    * @param fps    Frames per second to limit the game to
    * @return Engine|null instance
    */
   public static Engine instance(JFrame window, int fps) {
      // If we do not have an instance, create a new Engine instance
      if (instance == null) {
         // If we were not given a render window, create our own
         log("No render component set! Engine is creating it's own...", true);
         // Create the instance
         instance = new Engine(window, fps);
      }
      return instance;
   }

   /**
    * Creates an Engine instance and initializes the Engine
    *
    * @param window JFrame to render on
    * @param fps    Frame per second to limit the game to
    */
   protected Engine(JFrame window, int fps) {
      // Calculate the time offset we have to render
      // a given frame
      this.delta = 1.0 / fps;
      if (window == null)
         window = new JFrame();
      this.window = window;
   }

   /**
    * Initialize the Engine, all internal initialization code
    * should go here
    */
   private void init() {
      // Get the screen size so we know how large of a window to render
      GraphicsDevice dev = GraphicsEnvironment.
         getLocalGraphicsEnvironment().
         getDefaultScreenDevice();

      if (screenSize == null) {
         screenSize = new Dimension();
         screenSize.setSize(
            dev.getDisplayMode().getWidth(),
            dev.getDisplayMode().getHeight()
         );
      }
      window.setResizable(false);
      window.getContentPane().setBackground(Color.black);
      window.setUndecorated(isFullscreen());

      // Force Windowed mode
      if (isDebug()) {
         fullscreen = false;
      }

      if (!isFullscreen()) {
         // Windowed
         window.getContentPane().setPreferredSize(screenSize);
         window.pack();
         window.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
         window.setLocationRelativeTo(null);
         window.addWindowListener(new WindowListener() {
            @Override
            public void windowClosing(WindowEvent e) {
               if (onEscape != null)
                  onEscape.run();
               else
                  stop();
            }

            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
         });
      } else {
         // Full Screen
         window.setIgnoreRepaint(true);
         dev.setFullScreenWindow(window);
      }

      // Setup a key listener so we can handle keyboard events
      window.addKeyListener(this);
      window.setVisible(true);
   }

   /**
    * Destroy the Engine, hide the window and stop rendering
    */
   private void destroy() {
      GraphicsDevice dev = GraphicsEnvironment
         .getLocalGraphicsEnvironment()
         .getDefaultScreenDevice();

      dev.setFullScreenWindow(null);
      window.removeKeyListener(this);
      window.setVisible(false);
      window.dispose();

      keys = new ArrayList<>();
      actors = new CopyOnWriteArrayList<>();

      if (onDestroy != null)
         onDestroy.run();
   }

   //endregion

   //region Getters and Setters

   /**
    * Get accurate time in seconds
    *
    * @return Current time
    */
   private double getTime() {
      // Get time and convert to seconds
      return (double) System.nanoTime() / 1000000000.0;
   }

   /**
    * Get the keys that are currently pressed
    *
    * @return List of pressed keys
    */
   public List<KeyEvent> getPressedKeys() {
      return keys;
   }

   /**
    * Gets the value of fullscreen
    *
    * @return true|false
    */
   public boolean isFullscreen() {
      return fullscreen;
   }

   /**
    * Sets the value of fullscreen
    *
    * @param fullscreen true|false
    * @return Engine
    */
   public Engine setFullscreen(boolean fullscreen) {
      this.fullscreen = fullscreen;
      return this;
   }

   public Engine setSize(Dimension size) {
      this.setFullscreen(false);
      this.screenSize = size;
      return this;
   }

   /**
    * Set a callback when the Engine is stopped
    *
    * @param r Method to run
    */
   public Engine setOnDestroy(Runnable r) {
      this.onDestroy = r;
      return instance;
   }

   /**
    * Set a callback when the Engine is stopped
    *
    * @param r Method to run
    */
   public Engine setOnEscape(Runnable r) {
      this.onEscape = r;
      return instance;
   }

   /**
    * Tells the Engine to display the current
    * FPS on the screen or not. Used for debugging
    *
    * @param value true|false Flag
    */
   public Engine setDebug(boolean value) {
      this.debug = value;
      return instance;
   }

   /**
    * Gets the debug value
    *
    * @return true|false
    */
   public boolean isDebug() {
      return debug;
   }

   /**
    * Adds an Actor to the Engine for it to manage
    * will be called for rendering, input, collision,
    * and frame updates
    *
    * @param actor
    */
   public Engine addActor(Actor actor) {
      actors.add(actor);
      return instance;
   }

   /**
    * Add Actors to the Engine
    *
    * @param actors
    */
   public Engine addActors(Actor[] actors) {
      for (Actor a : actors) {
         addActor(a);
      }
      return instance;
   }

   /**
    * Remove an Actor from the Engine when the Actor is
    * no longer used
    *
    * @param actor Actor
    * @return Engine
    */
   public Engine deleteActor(Actor actor) {
      int index = actors.indexOf(actor);
      deleteActor(index);
      return this;
   }

   /**
    * Remove an Actor from the Engine when the Actor is
    * no longer used
    *
    * @param index Index
    */
   public Engine deleteActor(int index) {
      actors.remove(index);
      return instance;
   }

   public boolean hasActor(Class type) {
      Iterator<Actor> i = actors.iterator();
      while (i.hasNext()) {
         Actor a = i.next();
         if (type.isInstance(a))
            return true;
      }

      return false;
   }

   public List<Actor> getActors() {
      return actors;
   }

   //endregion

   //region Input Handlers

   @Override
   public void keyPressed(KeyEvent e) {
      // Handle Escape event, close Engine
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE && this.onEscape != null) {
         this.onEscape.run();
      }
      //System.exit(0);
      // Search through all the keys to instance sure
      // we do not have duplicates
      // only one instance of a key should be pushed at a time
      Iterator<KeyEvent> i = keys.iterator();
      while (i.hasNext()) {
         KeyEvent k = i.next();
         // Do we already have this key stored?
         if (k.getKeyCode() == e.getKeyCode())
            return;
      }

      keys.add(e);
   }

   @Override
   public void keyTyped(KeyEvent e) {
   }

   @Override
   public void keyReleased(KeyEvent e) {
      // Search through all of the keys pressed down
      // if we have this key that was released
      // remove it
      Iterator<KeyEvent> i = keys.iterator();
      while (i.hasNext()) {
         KeyEvent k = i.next();
         if (k.getKeyCode() == e.getKeyCode())
            i.remove();
      }
   }

   //endregion

   /**
    * Wrapper for when ignore debug isn't needed
    *
    * @param message
    */
   public static void log(String message) {
      Engine.log(message, false);
   }

   /**
    * Aids in development and debugging
    *
    * @param message
    * @param ignoreDebug
    */
   public static void log(String message, boolean ignoreDebug) {
      String name;
      int line = 0;

      // Try to determine the class that called log
      try {
         StackTraceElement[] stackTraceElements = Thread
            .currentThread()
            .getStackTrace();

         name = stackTraceElements[2].getClassName();
         line = stackTraceElements[2].getLineNumber();
      } catch (Exception e) {
         name = "Engine";
      }

      // if they want the log regardless of debug
      // or if we are in debug display it
      if (ignoreDebug || Engine.instance().debug) {
         if (line != 0)
            System.out.println(
               String.format("%s (%d): %s", name, line, message)
            );
         else
            System.out.println(
               String.format("%s: %s", name, message)
            );
      }
   }

   //region Methods

   /**
    * Takeover application control and start the computation thread
    */
   public void start() {
      run = true;
      // Threads allow the UI to still receive input
      // and let us do in depth processing without
      // locking up the main UI thread
//        Thread thread = new Thread(() -> {
      // Run the main game loop
      loop();
//        });/
//        thread.start();
   }

   /**
    * Stops the Engine's computational thread
    */
   public void stop() {
      run = false;
   }

   /**
    * Main game loop
    */
   private void loop() {
      // Initialize the Engine
      init();

      // Get the time when the next frame is needed
      // for the first loop run this should be now
      double next = getTime();
      int skipped_frames = 1;

      while (run) {
         double current = getTime();

         // If the time gap is too large we are too far behind
         // force an update and render and resync times
         if ((current - next) > MAX_TIME_DIFF)
            next = current;

         // It is time to render the next frame
         if (current >= next) {
            // Calculate the next render time after this frame
            next += delta;
            tick();

            // If we need to render, then do it and compute fps
            if ((current < next) || (skipped_frames > MAX_FRAME_SKIP)) {
               render();
               computeFPS();

               // We have rendered, reset skipped frames
               skipped_frames = 1;
            } else {
               // It is too soon to render, skip this frame
               skipped_frames++;
            }
         } else {
            // We are ahead of schedule and do not need to render
            // the next frame yet. Sleep until we do to save CPU cycles
            int sleep = (int) (1000.0 * (next - current));

            // Do we actually have time to sleep?
            if (sleep > 0) {
               try {
                  // Pause execution until the next render time
                  Thread.sleep(sleep);
               } catch (InterruptedException e) {
                  // We don't need to do anything here
               }
            }
         }
      }

      // If we are not running then the engine should be destroyed
      destroy();
   }

   /**
    * Determines if an actor is visible
    *
    * @param a
    * @return
    */
   private boolean isOnScreen(Actor a) {
      Rectangle r = a.getBoundingBox();
      Rectangle screen = new Rectangle(screenSize);
      return screen.intersects(r);
   }

   /**
    * Occurs on Engine frame updates calls each Actors
    * tick method. Put all heavy processing code in
    * the respective actor's tick method
    */
   private void tick() {
      double currentTick = System.currentTimeMillis();
      // If this is the first frame interpolation will be 0
      if (lastTick == 0)
         lastTick = currentTick;

      double interpolation = (currentTick - lastTick) / 1000;
      Iterator<Actor> i = actors.iterator();
      while (i.hasNext()) {
         Actor actor = i.next();

         // Don't waste resources on actors that are not even on the
         // screen or do not want to handle collisions
         if (actor.handlesCollision() && isOnScreen(actor)) {
            // Check for collision with other actors
            Iterator<Actor> j = actors.iterator();
            while (j.hasNext()) {
               Actor c = j.next();

               // Make sure we don't try to collide the actor with itself
               // the actor must also be visible on the screen
               // instance sure the actors are intersecting
               if (!c.equals(actor) && isOnScreen(c)
                  && actor.getBoundingBox()
                  .intersects(c.getBoundingBox())) {
                  actor.collide(c, false);
               }
            }
         }

         // For keys pushed, hand off to the actor
         if (keys.size() > 0)
            actor.input(keys, interpolation);

         // call actor tick
         actor.tick(interpolation);
      }


      lastTick = currentTick;
   }

   /**
    * Renders out the current frame using a double buffer method.
    * Render all the individual actors offscreen in a buffer and copy
    * that rendered frame onto the screen for viewing
    */
   private void render() {
      // Create an image offscreen the size of the screen we can render
      // multiple changes on. Reduces tear and lag this way
      int width = (int) screenSize.getWidth();
      int height = (int) screenSize.getHeight();

      BufferedImage img = new BufferedImage(
         width, height, BufferedImage.TYPE_INT_RGB
      );

      // Get a graphics object for the component we want to render on
      Graphics graphics;
      if (!isFullscreen()) {
         graphics = window.getContentPane().getGraphics();
      } else {
         graphics = window.getGraphics();
      }

      // Get a graphics object for our offscreen buffer
      Graphics2D buffer = img.createGraphics();
      try {
         // Set the buffer to black background and fill the entire screen
         buffer.setColor(Color.black);
         buffer.fillRect(0, 0, width, height);

         // Render each individual actor by passing in the buffer
         // the reason we do this on the actor class is to that they
         // can handle any special rendering cases for their implementation
         // without digging into the engine code. However, we can still provide
         // a default render method in the base Actor class this way.
         for (Actor actor : actors) {
            actor.render(buffer);
         }

         // If we want to show the current FPS on screen we can do that here
         if (debug) {
            buffer.setColor(Color.white);
            buffer.drawString(String.format("FPS: %.2f", currentFPS), 10, 10);
            log(String.format(
               "Render at: %s, FPS: %s", lastTick, currentFPS
            ));
         }

         // All of the objects should be rendered now in the buffer,
         // lets copy that image to the screen
         graphics.drawImage(img, 0, 0, null);
      } finally {
         // Cleanup, let garbage collection know we are done and force the
         // render
         if (graphics != null)
            graphics.dispose();

         if (buffer != null)
            buffer.dispose();
      }
   }

   /**
    * Calculates the current FPS that the Engine is running at
    */
   private void computeFPS() {
      // Get the current time
      double currentTime = System.currentTimeMillis();
      // If we have not counted any frames yet, this is the first frame
      // save the time
      if (frames == 0)
         startTime = currentTime;

      // We should only calculate FPS after a second has gone by
      if ((currentTime - startTime) >= 1000) {
         // Calculate our FPS
         currentFPS = 1 / ((currentTime - startTime) / frames) * 1000;
         // Reset our rendered frame count
         frames = 0;
         // We are starting over. Force a time reset
         return;
      }
      // This tick has been processed
      frames++;
   }
   //endregion
}
