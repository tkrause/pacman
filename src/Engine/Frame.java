package Engine;

//region Imports

import java.awt.image.BufferedImage;
//endregion

/**
 * Represents a single frame in an animation
 */
public class Frame {
   //region Properties
   /**
    * The image for this frame, known as a sprite
    */
   protected BufferedImage sprite;
   /**
    * This disposal method for this frame
    */
   protected String disposal;
   //endregion

   //region Initialization

   /**
    * Creates a frame with a sprite
    * disposal method default is empty string or Unspecified
    *
    * @param sprite The sprite
    */
   public Frame(BufferedImage sprite) {
      this(sprite, "");
   }

   /**
    * Creates a frame with a sprite and disposal method
    *
    * @param sprite   The sprite to render for this frame
    * @param disposal The method we should use to dispose
    *                 of this frame. Possible values are
    *                 - Unspecified (Same as No Disposal)
    *                 - No Disposal (any value other than the two below)
    *                 - Dispose to Background (restoreToBackgroundColor)
    *                 - Dispose to Previous (restoreToPrevious)
    *                 See http://www.theimage.com/animation/pages/disposal.html
    */
   public Frame(BufferedImage sprite, String disposal) {
      this.sprite = sprite;
      this.disposal = disposal;
   }
   //endregion

   //region Getters

   /**
    * Gets the sprite/image for this frame
    *
    * @return The image for this frame
    */
   public BufferedImage getSprite() {
      return sprite;
   }

   /**
    * Get the method that was used to create
    * this frame. Primarily used by State
    *
    * @return The disposal method
    */
   public String getDisposal() {
      return disposal;
   }
   //endregion

   //region Setters

   /**
    * Sets the sprite for this frame
    *
    * @param sprite The image to display
    */
   public void setSprite(BufferedImage sprite) {
      this.sprite = sprite;
   }
   //endregion
}
