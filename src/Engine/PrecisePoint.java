package Engine;

import java.awt.*;

/**
 * Represents a Point in space that is precise
 */
public class PrecisePoint {
   public double x, y;

   /**
    * Creates a PrecisePoint
    *
    * @param x X coordinate
    * @param y Y coordinate
    */
   public PrecisePoint(double x, double y) {
      this.x = x;
      this.y = y;
   }

   /**
    * Create a PrecisePoint from a Point
    *
    * @param p Point
    */
   public PrecisePoint(Point p) {
      if (p != null) {
         this.x = p.x;
         this.y = p.y;
      }
   }

   /**
    * Gets the x coordinate
    *
    * @return x
    */
   public double getX() {
      return x;
   }

   /**
    * Sets the x coordinate
    *
    * @param x x
    */
   public void setX(double x) {
      this.x = x;
   }

   /**
    * Gets the y coordinate
    *
    * @return y
    */
   public double getY() {
      return y;
   }

   /**
    * Sets the y coordinate
    *
    * @param y y
    */
   public void setY(double y) {
      this.y = y;
   }

   /**
    * Converts a PrecisePoint into a Point
    *
    * @return Point
    */
   public Point toPoint() {
      Point p = new Point();
      p.setLocation(x, y);
      return p;
   }

   @Override
   public String toString() {
      return "PrecisePoint{" +
         "x=" + x +
         ", y=" + y +
         '}';
   }
}
