package Engine;

//region Imports

import com.sun.imageio.plugins.gif.GIFImageReader;
import com.sun.imageio.plugins.gif.GIFImageReaderSpi;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
//endregion

/**
 * Represents a series of frames that instance up an
 * animation for an Actor
 */
public class State {
   //region Properties

   /**
    * The frames that instance up this animation
    */
   protected List<Frame> frames;
   /**
    * The name of this state
    */
   protected String name;
   /**
    * Callback when this state ends, if set to null
    * state will loop endlessly
    */
   protected Runnable onDone = null;
   /**
    * Should the animation loop or not?
    */
   protected boolean loop = true;
   /**
    * Pauses the animation of the state, if set
    * state will not advance frames
    */
   protected boolean paused = false;

   //endregion

   //region Initialization

   /**
    * Create a new state
    *
    * @param name   Name of the state
    * @param sprite Path to the GIF asset
    */
   public State(String name, String sprite) {
      this(name, sprite, true, null);
   }

   /**
    * Create a new state
    *
    * @param name   Name of the state
    * @param sprite Path to the GIF asset
    * @param onDone Callback when state completed
    */
   public State(String name, String sprite,
          boolean loop, Runnable onDone) {
      this.name = name;
      this.onDone = onDone;
      this.loop = loop;
      frames = new ArrayList<>();
      resourceToFrames(sprite);
   }

   /**
    * Create a new state
    *
    * @param name   Name of the state
    * @param frames Frames for this state
    */
   public State(String name, List<Frame> frames) {
      this.name = name;
      this.frames = frames;
   }

   /**
    * Create a new state
    *
    * @param name   Name of the state
    * @param frames Frames for the animation
    * @param loop   true|false
    * @param onDone Callback when frames done
    */
   public State(String name, List<Frame> frames,
          boolean loop, Runnable onDone) {

      this.name = name;
      this.frames = frames;
      this.onDone = onDone;
      this.loop = loop;
   }

   //endregion

   //region Getters and Setters

   /**
    * Get a frame by the index of the frame
    *
    * @param frame Index of the frame to get
    * @return Frame The frame to return to the user
    */
   public Frame getFrame(int frame) {
      return frames.get(frame);
   }

   /**
    * Get the name of this state instance
    *
    * @return The name of the state
    */
   public String getName() {
      return name;
   }

   /**
    * Get the number of frames in this state
    *
    * @return The number of frames
    */
   public int getFrameCount() {
      return frames.size();
   }

   /**
    * Gets the onDone handler
    *
    * @return onDone Handler
    */
   public Runnable getOnDone() {
      return onDone;
   }

   /**
    * Does the state have an onDone handler
    *
    * @return onDone true|false
    */
   public boolean hasOnDone() {
      return onDone != null;
   }

   /**
    * Sets the onDone handler
    *
    * @param onDone The code to run when state is complete
    */
   public void setOnDone(Runnable onDone) {
      this.onDone = onDone;
   }

   /**
    * Gets the value of paused
    *
    * @return isPaused
    */
   public boolean isPaused() {
      return paused;
   }

   /**
    * Sets the value of paused
    *
    * @param paused true|false
    */
   public void setPaused(boolean paused) {
      this.paused = paused;
   }

   /**
    * Gets the value of loop
    *
    * @return true|false Can loop?
    */
   public boolean canLoop() {
      return loop;
   }

   /**
    * Sets the value of loop
    *
    * @param loop true|false
    */
   public void setLoop(boolean loop) {
      this.loop = loop;
   }

   //endregion

   //region Methods

   /**
    * Converts a GIF image into individual frames.
    * Based on ideas from the link below
    * http://stackoverflow.com/questions/8933893/convert-each-animated-gif-frame-to-a-separate-bufferedimage
    *
    * @param resource Path to the resource
    */
   public void resourceToFrames(String resource) {
      try {
         // Get the full path to the resource
            /*String path = getClass().getResource(resource).getFile();
            File f = new File(path);*/

         // Open the image for reading
         ImageReader reader = new GIFImageReader(new GIFImageReaderSpi());
         reader.setInput(ImageIO.createImageInputStream(getClass()
            .getResourceAsStream(resource))
         );
         // Defaults
         int width = -1;
         int height = -1;
         // Retrieve the metadata for the image
         IIOMetadata meta = reader.getStreamMetadata();
         if (meta != null) {
            // Get the image size of the GIF if it is available
            IIOMetadataNode globalRoot =
               (IIOMetadataNode) meta.getAsTree(
                  meta.getNativeMetadataFormatName()
               );

            NodeList descriptor = globalRoot
               .getElementsByTagName("LogicalScreenDescriptor");

            if (descriptor != null && descriptor.getLength() > 0) {
               IIOMetadataNode screenDescriptor =
                  (IIOMetadataNode) descriptor.item(0);

               if (screenDescriptor != null) {
                  // Set the width and height of the GIF image
                  width = Integer.parseInt(
                     screenDescriptor.getAttribute("logicalScreenWidth")
                  );
                  height = Integer.parseInt(
                     screenDescriptor.getAttribute("logicalScreenHeight")
                  );
               }
            }
         }

         // For GIF images, since they are layered on top of each
         // other we will need to render them off screen and copy them
         // into our frames array
         BufferedImage buffer = null;
         Graphics2D graphics = null;

         // Foreach frame in the GIF
         for (int frame = 0; ; frame++) {
            BufferedImage image;

            try {
               // Read the frame, on error skip this frame
               image = reader.read(frame);
            } catch (IndexOutOfBoundsException io) {
               break;
            }

            // If we were not able get the height and width from
            // metadata we will use the size of the first frame
            if (width == -1 || height == -1) {
               width = image.getWidth();
               height = image.getHeight();
            }

            // GIF Images have a "Disposal Method" which tells us
            // how we should render the transparent images
            // typically they are layered over each other
            // without this, we will get partial images that won't look right
            IIOMetadataNode root = (IIOMetadataNode)
               reader.getImageMetadata(frame)
                  .getAsTree("javax_imageio_gif_image_1.0");
            IIOMetadataNode gce = (IIOMetadataNode)
               root.getElementsByTagName("GraphicControlExtension").item(0);
            String disposal = gce.getAttribute("disposalMethod");

            int x = 0, y = 0;
            // We have two options here, layer over the last render frame
            // or we will build an entirely new frame. This is decided below
            // by disposal method

            if (buffer == null) {
               // Create a new offscreen image with a transparent background to
               // paint on
               buffer = new BufferedImage(
                  width, height, BufferedImage.TYPE_INT_ARGB
               );
               graphics = buffer.createGraphics();
               graphics.setBackground(new Color(0, 0, 0, 0));
            } else {
               // GIF frames can be different sized if they are not optimized
               // thus we need to offset the image where we are told
               // to do so, we need to look at metadata
               NodeList children = root.getChildNodes();
               for (int node = 0; node < children.getLength(); node++) {
                  Node item = children.item(node);
                  if (item.getNodeName().equals("ImageDescriptor")) {
                     // Set the offset location
                     NamedNodeMap map = item.getAttributes();
                     x = Integer.valueOf(
                        map.getNamedItem("imageLeftPosition").getNodeValue()
                     );
                     y = Integer.valueOf(
                        map.getNamedItem("imageTopPosition").getNodeValue()
                     );
                  }
               }
            }

            // Render the frame and copy it into our frames array
            graphics.drawImage(image, x, y, null);
            BufferedImage copy = new BufferedImage(
               buffer.getColorModel(), buffer.copyData(null),
               buffer.isAlphaPremultiplied(), null
            );
            frames.add(new Frame(copy, disposal));

            // Handle the disposal method. Should we keep
            // this frame in buffer for the next render?
            if (disposal.equals("restoreToPrevious")) {
               // Instead of the frame we just render
               // the GIF has requested the frame before this one as the
               // layer to render on
               BufferedImage from = null;

               for (int i = frame - 1; i >= 0; i--) {
                  // Find previous frame to restore. If they all have a
                  // restoreToPrevious disposal method
                  // or we have no more frames, we will use the first
                  // frame
                  if (!frames.get(i).getDisposal()
                        .equals("restoreToPrevious") || frame == 0) {

                     from = frames.get(i).getSprite();
                     break;
                  }
               }

               // Load the frame we found into the main buffer for the
               // next frame to use
               buffer = new BufferedImage(
                  from.getColorModel(), from.copyData(null),
                  from.isAlphaPremultiplied(), null
               );
               graphics = buffer.createGraphics();
               graphics.setBackground(new Color(0, 0, 0, 0));
            } else if (disposal.equals("restoreToBackgroundColor")) {
               // Remove the area requested leaving the background color
               // showing through
               graphics.clearRect(x, y, image.getWidth(), image.getHeight());
            }
         }
         // Trash the reader we are done
         reader.dispose();
      } catch (IOException e) {
         // We may want to close the game engine here
         // currently we handle this gracefully and no asset will display
         // but log an error for the developer
         System.out.println("Error loading resource " + resource);
         e.printStackTrace();
      }
   }
   //endregion
}
